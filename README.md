University of the Pacific's Meta-Classifier
===

This repository contains the code and documentation for Pacific's meta-classification system

---

## About

The Meta-Classifier is an end-to-end system that collects data, performs feature extraction, finds the best classifier for the data, and programs the selected model to a robot for future classification tasks.
It was designed to help researchers use machine learning to solve problems without requiring a large human time investment during setup.
This project began as part of a master's thesis by Tristan Watts-Willis in 2016.

---

## Organization

The main repository is split among three folders.

* [Workstation](Workstation): Contains the data processing, classifier training, and model selection scripts
	* [timeFeatures.py](#markdown-header-timefeatures): Extracts features from time series data
	* [meanOverlap.py](#markdown-header-meanoverlap): Analyzes data file and outputs metric of uniqueness of each feature
	* [filterMeanOverlap.py](#markdown-header-filtermeanoverlap): Extracts features from time series data and performs basic feature filtering
	* [multiClassify.py](#markdown-header-multiclassify): Trains classifiers and selects best model
	* [fullIntegration.py](#markdown-header-fullintegration): Combines functionality of above scripts into one for streamlined model creation
* [Pi](Pi): Contains the scripts required for on-board data collection and running the selected classifier
* [Data](Data): Contains data collected during the development and testing of the system. Includes raw and post-processed data.
	
---

## Prerequisites

This system was designed and tested with the following software on Windows 10 64-bit, Ubuntu 14.04 64-bit, and Raspbian 8.1 32-bit on a Raspberry Pi 3.

* [Python 3.5.0](https://www.python.org/)
* [Java JRE/JDK 1.8.0_101](https://www.java.com/en/download/)
* [WEKA 3.8.0](http://www.cs.waikato.ac.nz/~ml/weka/index.html)
* [JavaBridge 1.0.14](http://pythonhosted.org/javabridge/) (Installable with pip)
* [Python-WEKA-Wrapper 0.3.10](http://pythonhosted.org/python-weka-wrapper/) (Installable with pip)
* [SciPy 0.18.0](https://www.scipy.org/)

Note: JavaBridge does not successfully install by default on Raspbian because it is not configured to search for the Java ARM libraries. To workaround this problem, create a symlink to the arm folder called amd64: ```jdk1.8.0_101/jre/lib/amd64 -> jdk1.8.0_101/jre/lib/arm```

---

## Usage

### timeFeatures

Extracts features from time series data. This scripts expects CSV input with each sample in a new row. The first column must be the classification of the sample with the following columns containing interleaved data from sensors. The user can specify the sensor names to have more meaningful feature names. In the resulting file, the raw data columns are replaced with the columns representing each feature.

```
usage: timeFeatures.py [-h] [-s STRIDE] [-r SAMPLERATE] [-n NAMES] file

extract features from time series data

positional arguments:
  file                  the data file

optional arguments:
  -h, --help            show this help message and exit
  -s STRIDE, --stride STRIDE
                        the stride count, if series contains multiple sources
  -r SAMPLERATE, --samplerate SAMPLERATE
                        the sample rate of the data
  -n NAMES, --names NAMES
                        names of sensors
```

### meanOverlap

Analyzes features in the input data file and outputs our mean overlap metric for each. The mean overlap metric represents the amount of overlap between different classification categories in the data (lower is better).

```
usage: meanOverlap.py [-h] [-o MAXOVERLAP] file

determine classifiability of features in a dataset

positional arguments:
  file                  the data file

optional arguments:
  -h, --help            show this help message and exit
  -o MAXOVERLAP, --maxoverlap MAXOVERLAP
                        filter out features with higher mean overlap than this
```

### filterMeanOverlap

This script extracts features from time series data. The input file should be CSV with the first column as the classification of the sample and the following columns as interleaved time series data from each sensor. The features are then processed through our mean overlap algorithm to give us a metric of how useful it is. The best features are kept and saved into a new file. The number of features to keep is specified with KEEPFEATURES. If this number is a value between 0 and 1, it will be interpreted as a percentage of features to keep; e.g. 0.5 would keep the top 50% best features.

```
usage: filterMeanOverlap.py [-h] [-n SENSORNAMES] [-r SAMPLERATE]
                            [-kf KEEPFEATURES]
                            file

extracts feature from time series data and saves the processed data to a new
file with only the best features

positional arguments:
  file                  the data file

optional arguments:
  -h, --help            show this help message and exit
  -n SENSORNAMES, --sensornames SENSORNAMES
                        list of sensor names in same order as source data
  -r SAMPLERATE, --samplerate SAMPLERATE
                        rate at which sensors were sampled in Hz
  -kf KEEPFEATURES, --keepfeatures KEEPFEATURES
                        the number of features to keep from mean overlap
```

### multiClassify

This script forms the bulk of the system. It trains classifiers for every combinations of input features and classification algorithms. Each model is evaluated using cross-validation and separate verification data. The list of models is evaluated against several user-supplied parameters to determine the best one. This best model, along with statistics for all generated models, is saved under a folder with the same name as the input file. The model file is a WEKA model.

```
usage: multiClassify.py [-h] [-vf VERIFICATIONFILE] [-cl CLASSIFIERLIST]
                        [-mx MAXFEATURES] [-mn MINFEATURES] [-p PERCENTDROP]
                        [-c CLASSVAR] [-cc CRITICALCLASS] [-fp FALSEPOSITIVES]
                        [-f FOLDS] [-v]
                        file

positional arguments:
  file                  the data file

optional arguments:
  -h, --help            show this help message and exit
  -vf VERIFICATIONFILE, --verificationfile VERIFICATIONFILE
                        second data file to be used for verification
  -cl CLASSIFIERLIST, --classifierlist CLASSIFIERLIST
                        file containing list of classifiers to be used,
                        seperated by newline
  -mx MAXFEATURES, --maxfeatures MAXFEATURES
                        the maximum number of features to use per classifier
  -mn MINFEATURES, --minfeatures MINFEATURES
                        the minimum number of features to use per classifier
  -p PERCENTDROP, --percentdrop PERCENTDROP
                        the percentage allowed to drop from max accuracy to
                        find more efficient classifier
  -c CLASSVAR, --classvar CLASSVAR
                        the variable name to be used as the class
  -cc CRITICALCLASS, --criticalclass CRITICALCLASS
                        the class value to be used when calculating true/false
                        positives
  -fp FALSEPOSITIVES, --falsepositives FALSEPOSITIVES
                        the maximum percent of allowed false positives
  -f FOLDS, --folds FOLDS
                        the number of folds to use during cross-validation
  -v, --verbose         enables java errors and extra error logging
```

### fullIntegration

Combines functionality from timeFeatures, meanOverlap, and multiClassify. Accepts input raw data, extracts features, removes bad features, trains classifier, and outputs the best model in WEKA format.

```
usage: fullIntegration.py [-h] [-n SENSORNAMES] [-r SAMPLERATE]
                          [-vf VERIFICATIONFILE] [-cl CLASSIFIERLIST]
                          [-mx MAXFEATURES] [-mn MINFEATURES]
                          [-kf KEEPFEATURES] [-p PERCENTDROP] [-c CLASSVAR]
                          [-cc CRITICALCLASS] [-fp FALSEPOSITIVES] [-f FOLDS]
                          [-v]
                          file

positional arguments:
  file                  the data file

optional arguments:
  -h, --help            show this help message and exit
  -n SENSORNAMES, --sensornames SENSORNAMES
                        list of sensor names in same order as source data
  -r SAMPLERATE, --samplerate SAMPLERATE
                        rate at which sensors were sampled in Hz
  -vf VERIFICATIONFILE, --verificationfile VERIFICATIONFILE
                        second data file to be used for verification
  -cl CLASSIFIERLIST, --classifierlist CLASSIFIERLIST
                        file containing list of classifiers to be used,
                        seperated by newline
  -mx MAXFEATURES, --maxfeatures MAXFEATURES
                        the maximum number of features to use per classifier
  -mn MINFEATURES, --minfeatures MINFEATURES
                        the minimum number of features to use per classifier
  -kf KEEPFEATURES, --keepfeatures KEEPFEATURES
                        the number of features to keep from mena overlap
  -p PERCENTDROP, --percentdrop PERCENTDROP
                        the percentage allowed to drop from max accuracy to
                        find more efficient classifier
  -c CLASSVAR, --classvar CLASSVAR
                        the variable name to be used as the class
  -cc CRITICALCLASS, --criticalclass CRITICALCLASS
                        the class value to be used when calculating true/false
                        positives
  -fp FALSEPOSITIVES, --falsepositives FALSEPOSITIVES
                        the maximum percent of allowed false positives
  -f FOLDS, --folds FOLDS
                        the number of folds to use during cross-validation
  -v, --verbose         enables java errors and extra error logging
```