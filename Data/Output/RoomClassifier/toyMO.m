%%
%tempmean column 11
close all
tmeans=[];
tvars=[];
lmeans=[];
lvars=[];
classes = 2;
features = 3;
featurenames = {'class','High Overlap','Low Overlap'};
overlaps = zeros(features-1,1);
for f=2:features
	temps = zeros(classes,100);
	if f==2
		temps(1,:) = normpdf(1:100,43,9);
		temps(1,:) = temps(1,:)/sum(temps(1,:));
		temps(2,:) = normpdf(1:100,57,9);
		temps(2,:) = temps(2,:)/sum(temps(2,:));
	elseif f==3
		temps(1,:) = normpdf(1:100,35,8);
		temps(1,:) = temps(1,:)/sum(temps(1,:));
		temps(2,:) = normpdf(1:100,65,8);
		temps(2,:) = temps(2,:)/sum(temps(2,:));
	end
	
	figure;hold on;
	for i=1:classes
		plot(temps(i,:));
	end
	
	i=1;
	sm=zeros(classes,1); % sum of myself
	su=zeros(classes,1); % sum of only myself
	overlap=zeros(classes,1);
	for i=1:classes
		for j=1:size(temps,2)
			sm(i) = sm(i)+temps(i,j);
			c = temps(i,j)>temps(:,j);
			if sum(c)==classes-1 % only smaller signals
				su(i) = su(i)+temps(i,j)-max(temps(c,j));
			end
		end
		overlap(i)=sm(i)-su(i);
	end
	overlaps(f-1)=mean(overlap);
	title(['Example ' featurenames{f} sprintf('\nMean Overlap:') num2str(overlaps(f-1))]);
	ylabel('Normalized Count');
	xlabel('Normalized Feature Value');
	legend('Category 1','Category 2');
end
% tmeans
% tvars
% lmeans
% lvars