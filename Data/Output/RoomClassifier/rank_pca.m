dataset = train2;

classes = max(dataset(:,1))+1;
features = size(dataset,2);
ranks = [];

for f=2:features
	len = Inf;
	for c=1:classes
		tmp = dataset(dataset(:,1)==c-1,f);
		len = min([size(tmp,1) len]);
	end
	dat = zeros(classes,len);
	for c=1:classes
		tmp = dataset(dataset(:,1)==c-1,f);
		dat(c,:) = tmp(1:len);
	end
	ranks(f-1)=rank(dat);
% 	pca(dat)
end