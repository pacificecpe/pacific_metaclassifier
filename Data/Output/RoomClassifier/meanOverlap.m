%%
%tempmean column 11
close all
tmeans=[];
tvars=[];
lmeans=[];
lvars=[];
dataset = exp4_m;
classes = max(dataset(:,1))+1;
features = size(dataset,2);
overlaps = zeros(features-1,1);
medoverlaps = zeros(features-1,1);
spreads = zeros(features-1,1);
for f=2:features
	temps = zeros(classes,100);
	for i=1:classes
		tempmean = dataset(dataset(:,1)==i-1,f);
		tmin = min(dataset(:,f));
		tmax = max(dataset(:,f));
		for j=1:length(tempmean)
			indx = min(100,1+floor((tempmean(j)-tmin)*(100/(tmax-tmin))));
			temps(i,indx)=temps(i,indx)+1;
		end
		temps(i,:) = temps(i,:)/length(tempmean);
	end
	
	figure;hold on;
	for i=1:classes
		plot(temps(i,:));
	end
	
	i=1;
	sm=zeros(classes,1); % sum of myself
	su=zeros(classes,1); % sum of only myself
	overlap=zeros(classes,1);
	for i=1:classes
		for j=1:size(temps,2)
			sm(i) = sm(i)+temps(i,j);
			c = temps(i,j)>temps(:,j);
			if sum(c)==classes-1 % only smaller signals
				su(i) = su(i)+temps(i,j)-max(temps(c,j));
			end
		end
		overlap(i)=sm(i)-su(i);
	end
	overlaps(f-1)=mean(overlap);
	medoverlaps(f-1)=median(overlap);
	spread = abs(mean(dataset(:,f))-median(dataset(:,f)))/(max(dataset(:,f))-min(dataset(:,f)));
	spreads(f-1)=spread;
	%title(['Feature: ' featurenames{f} sprintf('\nMean Overlap:') num2str(overlaps(f-1))]);
	title(sprintf('Feature: %s\nMean Overlap: %f  Median Overlap: %f\nMean Val: %f  Median Val: %f\nSpread: %f', ... 
		featurenames{f},overlaps(f-1),medoverlaps(f-1),mean(dataset(:,f)),median(dataset(:,f)),spread));
end
% tmeans
% tvars
% lmeans
% lvars


