%% Import Data File
close all;
filename = '.\3ClassTest_train_results.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%*q%*q%f%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
data = [dataArray{1:end-1}];
clearvars filename delimiter startRow formatSpec fileID dataArray ans;
nfeatures = max(data(:,1));

%% Classification Rates
figure;
data = sortrows(data,3);
classrate={};
for i=1:nfeatures
	classrate{i}=[];
	classhist{i}=zeros(100,1);
end

for i=1:size(data,1)
	fnum = data(i,1);
	classrate{fnum}(end+1) = data(i,3);
	classhist{fnum}(ceil(data(i,3))) = classhist{fnum}(ceil(data(i,3)))+1;
end

for i=1:nfeatures
	x = linspace(0,1,size(classrate{i},2));
	plot(x,classrate{i});
	hold on;
end
h=legend(num2str((1:nfeatures)'));
set(get(h,'title'),'string','Number of Features');
title('Overall Classification Rates');
ylabel('Classification Rate');
xlabel('Index');
saveas(gcf,'figs/classrate_plot.fig');

figure;
for i=1:nfeatures
	plot(classhist{i}/sum(classhist{i}));
	hold on;
end
h=legend(num2str((1:nfeatures)'));
set(get(h,'title'),'string','Number of Features');
title('Classification Rate Histogram');
ylabel('Normalized Count');
xlabel('Classification Rate');
saveas(gcf,'figs/classrate_hist.fig');

%% Verification Rates
figure;
data = sortrows(data,6);
classrate={};
for i=1:nfeatures
	classrate{i}=[];
	classhist{i}=zeros(100,1);
end

for i=1:size(data,1)
	fnum = data(i,1);
	classrate{fnum}(end+1) = data(i,6);
	classhist{fnum}(ceil(data(i,6))) = classhist{fnum}(ceil(data(i,6)))+1;
end

for i=1:nfeatures
	x = linspace(0,1,size(classrate{i},2));
	plot(x,classrate{i});
	hold on;
end
h=legend(num2str((1:nfeatures)'));
set(get(h,'title'),'string','Number of Features');
title('Overall Verification Rates');
ylabel('Classification Rate');
xlabel('Index');
saveas(gcf,'figs/verifrate_plot.fig');

figure;
for i=1:nfeatures
	plot(classhist{i}/sum(classhist{i}));
	hold on;
end
h=legend(num2str((1:nfeatures)'));
set(get(h,'title'),'string','Number of Features');
title('Verification Rate Histogram');
ylabel('Normalized Count');
xlabel('Classification Rate');
saveas(gcf,'figs/verifrate_hist.fig');

%% Classification Rate Box Chart
bdat=[];
bdat2={};
for i=1:nfeatures
	bdat(i)=mean(data(data(:,1)==i,3));
	bdat2{i}=data(data(:,1)==i,3);
end
% figure;
% bar(1:5,bdat);
% title('Classification by Number of Features');
% ylabel('Classification Rate');
% xlabel('Number of Features');
x=[];
g=[];
for i=1:nfeatures
	x = [x;bdat2{i}];
	g = [g;ones(length(bdat2{i}),1)*i];
end
% x = [bdat2{1};bdat2{2};bdat2{3};bdat2{4};bdat2{5}];
% g = [ones(length(bdat2{1}),1);ones(length(bdat2{2}),1)*2;ones(length(bdat2{3}),1)*3;...
% 	ones(length(bdat2{4}),1)*4;ones(length(bdat2{5}),1)*5];
figure;
boxplot(x,g);
hold on;
plot(1:nfeatures,bdat,'d');
title('Classification by Number of Features');
ylabel('Classification Rate');
xlabel('Number of Features');
saveas(gcf,'figs/classrate_box.fig');

%% Verification Rate Box Chart
bdat=[];
bdat2={};
for i=1:nfeatures
	bdat(i)=mean(data(data(:,1)==i,6));
	bdat2{i}=data(data(:,1)==i,6);
end
% figure;
% bar(1:5,bdat);
% title('Classification by Number of Features');
% ylabel('Classification Rate');
% xlabel('Number of Features');
x=[];
g=[];
for i=1:nfeatures
	x = [x;bdat2{i}];
	g = [g;ones(length(bdat2{i}),1)*i];
end
figure;
boxplot(x,g);
hold on;
plot(1:nfeatures,bdat,'d');
title('Verification by Number of Features');
ylabel('Classification Rate');
xlabel('Number of Features');
saveas(gcf,'figs/verifrate_box.fig');

%% Gen Dot Plots
data = sortrows(data,3);
figure;plot(data(:,6),'.');hold on;plot(data(:,3),'.');title('Classification Rates per Index');
xlabel('Classifier Index');
ylabel('Classification Rate');
legend('Verification Data','Training Data');