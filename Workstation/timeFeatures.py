# Extracts the following features from time series data:
# Max value, min value, peak frequency, signal variance, 2nd max, 2nd min, ratio, skewness, kurtosis, mean, settling time

import os.path
import argparse
import math
import numpy as np
import scipy
from scipy.signal import argrelextrema, argrelmax, argrelmin
from scipy.stats import skew, kurtosis
from collections import OrderedDict
import csv

def main():
    parser = argparse.ArgumentParser(description="extract features from time series data")
    parser.add_argument("file", help="the data file")
    parser.add_argument("-s","--stride", type=int, default=1, help="the stride count, if series contains multiple sources")
    parser.add_argument("-r","--samplerate", type=float, default=1.0, help="the sample rate of the data")
    parser.add_argument("-n","--names", type=str, default="", help="names of sensors")
    args = parser.parse_args()
    
    print("Loading data file...")
    filename = os.path.splitext(os.path.basename(args.file))[0]
    
    datas = []
    with open(args.file,'r') as csvfile:
        reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
        for row in reader:
            data = [row[0]]
            for n in range(1,args.stride+1):
                data.append(np.array(row[n::args.stride]).astype(np.float))
            datas.append(data)

    names = args.names.split(',')

    with open(filename+'_proc.csv','w') as csvfile:
        writer = csv.writer(csvfile,delimiter=',',lineterminator='\n')
        fields = ['max', 'min', 'peak-freq', 'variance', 'second-max', 'second-min', 'ratio',
            'skewness', 'kurtosis', 'mean', 'settling-time']
        fieldnames = ['class']
        for n in range(1,args.stride+1):
            for f in fields:
                if len(names)==args.stride:
                    fieldnames.append(names[n-1]+' '+f)
                else:
                    fieldnames.append(str(n)+' '+f)
        writer.writerow(fieldnames)
        i=1
        for data in datas:
            print("\rAnalyzing {} of {}...".format(i,len(datas)),end="")
            i+=1
            row = [data[0]]
            for timeset in data[1:]:
                smoothed=smooth(timeset,25,window='hamming')
                max = maxValue(smoothed)
                min = minValue(smoothed)
                peak = peakFreq(smoothed,args.samplerate)
                var = np.var(smoothed)
                skew = scipy.stats.skew(smoothed)
                kurtosis = scipy.stats.kurtosis(smoothed)
                settletime = settlingTime(smoothed,args.samplerate)
                row += [max[0],min[0],peak,var,max[1],min[1],max[0]/max[1],skew,kurtosis,np.mean(smoothed),settletime]
            writer.writerow(row)
        print()
        print("Done")

def getFeatureNames(stride,names):
    """Get feature names from sensor count and sensor names"""

    fields = ['max', 'min', 'peak-freq', 'variance', 'second-max', 'second-min', 'ratio',
        'skewness', 'kurtosis', 'mean', 'settling-time']
    fieldnames = ['class']
    for n in range(1,stride+1):
        for f in fields:
            if len(names)==stride:
                fieldnames.append(names[n-1]+' '+f)
            else:
                fieldnames.append(str(n)+' '+f)
    return fieldnames

def getFeatures(rawdata,stride,samplerate):
    """Extract features from rawdata using the provided sensor count and sample rate"""

    data = []
    for n in range(0,stride):
        data.append(np.array(rawdata[n::stride]).astype(np.float))
    row=[]
    for timeset in data:
        smoothed=smooth(timeset,25,window='hamming')
        max = maxValue(smoothed)
        min = minValue(smoothed)
        peak = peakFreq(smoothed,samplerate)
        var = np.var(smoothed)
        skew = scipy.stats.skew(smoothed)
        kurtosis = scipy.stats.kurtosis(smoothed)
        settletime = settlingTime(smoothed,samplerate)
        row += [max[0],min[0],peak,var,max[1],min[1],max[0]/max[1],skew,kurtosis,np.mean(smoothed),settletime]
    return row
	
def getFeaturesDict(rawdata,samplerate):
    timeset = np.array(rawdata).astype(np.float)

    fields = ['max', 'min', 'peak-freq', 'variance', 'second-max', 'second-min', 'ratio',
        'skewness', 'kurtosis', 'mean', 'settling-time']

    smoothed=smooth(timeset,25,window='hamming')
    max = maxValue(smoothed)
    min = minValue(smoothed)
    peak = peakFreq(smoothed,samplerate)
    var = np.var(smoothed)
    skew = scipy.stats.skew(smoothed)
    kurtosis = scipy.stats.kurtosis(smoothed)
    settletime = settlingTime(smoothed,samplerate)
    row = [max[0],min[0],peak,var,max[1],min[1],max[0]/max[1],skew,kurtosis,np.mean(smoothed),settletime]
    return dict(zip(fields,row))

# Smooth function from http://scipy-cookbook.readthedocs.io/items/SignalSmooth.html
def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')

    h=int(window_len/2)
    y=y[h:-h]

    return y

def maxValue(data):
    indxs = argrelmax(data,order=5)
    if len(indxs[0])<2:
        indxs = argrelextrema(data,np.greater_equal,order=5)
    maxes = data[indxs]
    maxes = sorted(maxes)
    if len(maxes)>1:
        return (maxes[-1],maxes[-2])
    else:
        return (maxes[0],maxes[0])

def minValue(data):
    indxs = argrelmin(data,order=5)
    if len(indxs[0])<2:
        indxs = argrelextrema(data,np.less_equal,order=5)
    mins = data[indxs]
    mins = sorted(mins)
    if len(mins)>1:
        return (mins[0],mins[1])
    else:
        return (mins[0],mins[0])

def peakFreq(data,sampleRate=1.0):
    data = data-np.mean(data)
    fft = np.absolute(np.fft.rfft(data))[5:-1]
    fftfreq = np.fft.rfftfreq(len(data),1.0/sampleRate)[5:-1]
    return fftfreq[np.argmax(fft)]

def settlingTime(data, sampleRate=1.0):
    mean = np.mean(data[-20:-1])
    data = data - mean
    max = argrelextrema(data,np.greater_equal)
    maxes = data[max]
    maxes = sorted(maxes)
    max = maxes[-1]
    indx = np.where(data==max)[0][0]
    for i in range(indx,len(data)):
        if data[i] < max/2:
            return (i-indx)/sampleRate
    return (len(data)-indx)/sampleRate
    
if __name__ == "__main__":
    main()