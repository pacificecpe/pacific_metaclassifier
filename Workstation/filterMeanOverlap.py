import multiClassify
import timeFeatures
import meanOverlap
import csv
import argparse
import os
import time
import tempfile

import weka.core.jvm as jvm
from weka.core.classes import Random
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization

parser = argparse.ArgumentParser(description="extracts feature from time series data and saves the processed data to a new file with only the best features")
parser.add_argument("file", help="the data file")
parser.add_argument("-n", "--sensornames", type=str, help="list of sensor names in same order as source data")
parser.add_argument("-r", "--samplerate", type=float, default=1.0, help="rate at which sensors were sampled in Hz")
parser.add_argument("-kf","--keepfeatures", type=float, default=0.5, help="the number of features to keep from mean overlap")
args = parser.parse_args()

filename = os.path.basename(args.file).split('.')[0]
test_file = args.file
temp_file = "{}_temp.csv".format(filename)
temp_file2 = "{}_rdy.arff".format(filename)

print("Loading data file and extracting features...")
data = []
names = None
if args.sensornames != None:
    sensornames = args.sensornames.split(',')
else:
    sensornames = []
with open(test_file,'r') as csvfile:
    reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
    for row in reader:
        if names == None:
            names = timeFeatures.getFeatureNames(len(sensornames),sensornames)
        data.append([row[0]]+timeFeatures.getFeatures(row[1:],len(sensornames),args.samplerate))

with open(temp_file,'w') as csvfile:
    writer = csv.writer(csvfile,delimiter=',',lineterminator='\n')
    fieldnames = names
    writer.writerow(fieldnames)
    for d in data:
        writer.writerow(d)

print("Extracted {} features, filtering using mean overlap...".format(len(names)))
mo_data = meanOverlap.processData(data)
overlap = meanOverlap.getOverlaps(mo_data,names)
overlap = sorted(overlap,key=lambda x: x[1])
numtokeep = min(len(overlap),int(args.keepfeatures)) if args.keepfeatures > 1 else int(len(overlap)*args.keepfeatures)
if numtokeep < 0:
    numtokeep = int(len(overlap)*0.5)
goodFeatures = [o[0] for o in overlap[0:numtokeep]]
print("Top {} good features:".format(numtokeep))
print(goodFeatures)

jvm.start(packages=True)
loader = Loader(classname="weka.core.converters.CSVLoader")
data = loader.load_file(temp_file)
data.class_index = 0
data = multiClassify.genDataSetWithAttributes(data,goodFeatures)

saver = Saver(classname="weka.core.converters.ArffSaver")
saver.save_file(data,temp_file2)

jvm.stop()