# http://pythonhosted.org/python-weka-wrapper/api.html

import time
import os

import weka.core.jvm as jvm
jvm.start()
os.system("cls")
print("Java VM initialized.\n")

from weka.core.classes import Random
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver

loader = Loader(classname="weka.core.converters.ArffLoader")
data = loader.load_file("./engr253finalData.arff")
data.class_index = 13
#print(data)

classifier = Classifier(classname="weka.classifiers.trees.J48")#, options = ["-C", "0.25", "-M", "2"])
#classifier = Classifier(classname="weka.classifiers.trees.RandomTree", options = ["-K", "0", "-M", "1.0", "-V", "0.001", "-S", "1"])
classTime = time.time()
classifier.build_classifier(data)
classTime = time.time()-classTime
print(classTime)
#import weka.plot.graph as graph
#graph.plot_dot_graph(classifier.graph)

evaluation = Evaluation(data)
evaluation.crossvalidate_model(classifier, data, 10, Random(time.time()))
#evaluation.evaluate_train_test_split(classifier,data,0.75,Random(time.time()))
#evaluation.test_model(classifier,data)
print(classifier)
print(evaluation.summary())
print("pctCorrect: "+str(evaluation.percent_correct))
print("incorrect: "+str(evaluation.percent_incorrect))
print(evaluation.confusion_matrix)
print(evaluation.class_details())
print(evaluation.area_under_roc(0))
print(evaluation.area_under_prc(0))

def createInstanceMixed(values, dataset):
    if not isinstance(values,list):
        return None
    if not isinstance(dataset,Instances):
        return None
        
    numericVals = []
    
    for indx, val in enumerate(values):
        if isinstance(val,str):
            val = dataset.attribute(indx).index_of(val)
        numericVals.append(val)

    inst = Instance.create_instance(numericVals)
    inst.dataset = dataset 
    return inst

valList = ['volvo','gas','turbo','four',4,'sedan','rwd','front',109.1,188.8,68.9,55.5,3062,
    'ohc','four',4,141,'mpfi',3.78,3.15,9.5,114,5400,19,25,22625]
inst = createInstanceMixed(valList,data)
print(inst)
indx = classifier.classify_instance(inst)
print(data.class_attribute.value(indx))
#inst = Instance.create_instance(['volvo','gas','turbo','four',4,'sedan','rwd','front',109.1,188.8,68.9,55.5,3062,
#    'ohc','four',4,141,'mpfi',3.78,3.15,9.5,114,5400,19,25,22625])
#print(inst)
#print(classifier.classify_instance(inst))

#for index, inst in enumerate(data):
#    print(inst)
#    print(classifier.classify_instance(inst))

import weka.plot.classifiers as plcls
plcls.plot_roc(evaluation, class_index=range(0,data.class_attribute.num_values), wait=True)
# plcls.plot_prc(evaluation, class_index=[0,1], wait=True)
#print(classifier.graph())
#print(data.get_instance(0))
#print(classifier.classify_instance(data.get_instance(0)))

jvm.stop()