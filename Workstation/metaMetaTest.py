import multiClassify
import timeFeatures
import meanOverlap
import csv
import argparse
import os
import time
import tempfile

import weka.core.jvm as jvm
from weka.core.classes import Random
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization

resultsFile = 'Work/3ClassTest_results2.csv'
outFile = 'Work/3ClassTest_results3.csv'
modelFile = 'Work/metaTest.model'

jvm.start(packages=True)
jobjects = serialization.read_all(modelFile)
dataset = Instances(jobject=jobjects[1])
print(dataset)
classifier = Classifier(jobject=jobjects[0])
print(classifier)

print("Reading results file...")
fieldnames = None
resultList = []
with open(resultsFile,'r') as csvfile:
    reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
    fieldnames = next(reader)
    for row in reader:
        ind = dataset.attribute(0).index_of(row[0])
        inst = Instance.create_instance([ind,0,row[8],row[9],row[10],row[11],row[12]])
        inst.dataset = dataset
        res = classifier.classify_instance(inst)
        row.extend([res])
        resultList.append(row)
fieldnames.extend(['Estimated Accuracy'])

print("Saving modified results file...")
with open(outFile,'w') as csvfile:
    writer = csv.writer(csvfile,delimiter=',',lineterminator='\n')
    writer.writerow(fieldnames)
    for d in resultList:
        writer.writerow(d)

jvm.stop()