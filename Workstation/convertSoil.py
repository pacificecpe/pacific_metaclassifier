import sys, os
import pathlib
import csv
import math

rootDir = "C:/Users/Tristan/Downloads/anthony_indoor_9_1_2014"
categories = ['carpet','foam','tile','wood']
samples = 50

setName = rootDir.split('/')[-1]
with open(rootDir+'/'+setName+'.csv','w') as outfile:
    writer = csv.writer(outfile,delimiter=',',lineterminator='\n')
    for category in categories:
        for sample in range(0,samples):
            try:
                with open('{0}/{1}_train/{1}_start_{2}.csv'.format(rootDir,category,sample),'r') as infile:
                    reader = csv.reader(infile,delimiter=',',lineterminator='\n')
                    next(reader)
                    row = next(reader)
                    startTime = float(row[3])+float(row[2])/1e9
            except:
                startTime = 0
            try:
                with open('{0}/{1}_train/{1}_stop_{2}.csv'.format(rootDir,category,sample),'r') as infile:
                    reader = csv.reader(infile,delimiter=',',lineterminator='\n')
                    next(reader)
                    row = next(reader)
                    stopTime = float(row[3])+float(row[2])/1e9
            except:
                stopTime = math.inf
            data = []
            with open('{0}/{1}_train/{1}_z_accel_{2}.csv'.format(rootDir,category,sample),'r') as infile:
                reader = csv.reader(infile,delimiter=',',lineterminator='\n')
                next(reader)
                for row in reader:
                    curTime = float(row[4])+float(row[3])/1e9
                    if curTime > startTime and curTime < stopTime:
                        data.append(int(row[0]))
            # print(startTime)
            # print(stopTime)
            # print(len(data))
            writer.writerow([category]+data)