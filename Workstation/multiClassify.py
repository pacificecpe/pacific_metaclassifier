#!/usr/bin/env python3
# http://pythonhosted.org/python-weka-wrapper/api.html

import sys
import time
import os
import argparse
import csv
import logging
from io import StringIO

import weka.core.jvm as jvm
from weka.core.classes import Random
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization

def main():
    parser = argparse.ArgumentParser(description="determine the best classifier algorithm and feature set for a given set of data")
    parser.add_argument("file", help="the data file")
    parser.add_argument("-vf","--verificationfile", type=str, help="second data file to be used for verification")
    parser.add_argument("-cl","--classifierlist", type=str, help="file containing list of classifiers to be used, seperated by newline")
    parser.add_argument("-mx","--maxfeatures", type=int, help="the maximum number of features to use per classifier")
    parser.add_argument("-mn","--minfeatures", type=int, help="the minimum number of features to use per classifier")
    parser.add_argument("-p","--percentdrop", type=float, default=0.0, help="the percentage allowed to drop from max accuracy to find more efficient classifier")
    parser.add_argument("-c","--classvar", type=str, help="the variable name to be used as the class")
    parser.add_argument("-cc","--criticalclass", type=str, help="the class value to be used when calculating true/false positives")
    parser.add_argument("-fp","--falsepositives", type=float, default=100.0, help="the maximum percent of allowed false positives")
    parser.add_argument("-f","--folds", type=int, default=5, help="the number of folds to use during cross-validation")
    parser.add_argument("-v","--verbose", default=False, action="store_true", help="enables java errors and extra error logging")
    args = parser.parse_args()

    for log in logging.Logger.manager.loggerDict:
        logger = logging.getLogger(log)
        logger.setLevel(logging.ERROR)

    jvm.start(packages=True)
    print("Java VM initialized.\n")

    if args.verbose:
        print("Running in verbose mode")
    else:
        print("Not running in verbose mode, suppressing Java errors")
        Joutputstream = jvm.javabridge.make_instance('java/io/ByteArrayOutputStream', "()V")
        Jprintstream = jvm.javabridge.make_instance('java/io/PrintStream', 
                                    "(Ljava/io/OutputStream;)V", Joutputstream)
        jvm.javabridge.static_call('Ljava/lang/System;', "setErr", 
                            "(Ljava/io/PrintStream;)V", Jprintstream)

    # Load specified data file and convert if needed
    filetype = os.path.basename(args.file).split('.')[1]
    try:
        if filetype == 'arff':
            filename = os.path.basename(args.file).split('.')[0]
            loader = Loader(classname="weka.core.converters.ArffLoader")
            data = loader.load_file(args.file)
            print("Loaded data file.")
        elif filetype == 'csv':
            filename = os.path.basename(args.file).split('.')[0]
            loader = Loader(classname="weka.core.converters.CSVLoader")
            data = loader.load_file(args.file)
            print("Loaded data file.")
        else:
            print('Unsupported filetype')
            exit()
    except:
        print("Error loading data file.")
        exit()

    # Assign class variable to specified variable, or default to first in data
    if args.classvar != None:
        attr = data.attribute_by_name(args.classvar)
        if attr == None:
            print("Invalid class attribute specified")
            data.class_index = 0
        else:
            data.class_index = attr.index
    else:
        data.class_index = 0
    print("Class attribute: {}".format(data.class_attribute))

    # Assign class variable value to use when determining false positives; defaults to first in data
    classVal = 0
    if args.criticalclass != None:
        if args.criticalclass in data.class_attribute.values:
            classVal = data.class_attribute.values.index(args.criticalclass)
    if data.class_attribute.values == None:
        print("Positive class value: {}".format(classVal))
    else:
        print("Positive class value: {}".format(data.class_attribute.values[classVal]))

    # Load verification file, if specified
    verifydata = None
    if args.verificationfile != None:
        try:
            verifydata = loader.load_file(args.verificationfile)
            print("Loaded verification data file.")
            verifydata.class_index = data.class_index
        except:
            print("Error loading verification data file.")
            verifydata = None
    else:
        print("No verification file specified.")

    # "weka.classifiers.meta.AutoWEKAClassifier"
    classifierList = ["weka.classifiers.functions.LDA","weka.classifiers.functions.QDA","weka.classifiers.functions.MultilayerPerceptron",
                "weka.classifiers.trees.J48","weka.classifiers.trees.RandomTree","weka.classifiers.trees.RandomForest",
                "weka.classifiers.bayes.NaiveBayes","weka.classifiers.rules.DecisionTable","weka.classifiers.meta.RandomSubSpace",
                "weka.classifiers.functions.Logistic","weka.classifiers.functions.SimpleLogistic", "weka.classifiers.functions.LibSVM"]
    if args.classifierlist != None:
        try:
            with open(args.classifierlist) as classifile:
                classifierlist = classifile.readlines()
            classifierList = [x.strip() for x in classifierlist]
            print("Loaded classifier list.")
        except:
            print("Error loading classifier list, using defaults.")
    else:
        print("No classifier list specified, using defaults.")
        
    print("Classifiers: {}".format(', '.join(classifierList)))
    
    print()

    attributeCombos = getFeatureCombos(data,args.maxfeatures,args.minfeatures)

    validationfolds = args.folds
    if not os.path.exists(filename):
        os.mkdir(filename)
    resultsList = generateModels(classifierList,attributeCombos,data,classVal,verifydata,validationfolds,filename)

    # Sort total results by training classification rate and save results as csv
    from operator import itemgetter
    resultsListSorted = sorted(resultsList,key=itemgetter(6))
    print("Storing results in {}_results.csv...\n".format(filename))
    with open(filename+"/"+filename+'_results.csv','w') as csvfile:
        wr = csv.writer(csvfile,lineterminator='\n')
        if verifydata != None and validationfolds > 0:
            wr.writerow(['Classifier','Feature Set','Feature Count','Train Time','Eval Time','Run Time','% Correct','% False Positive',
                'Verification % Correct', 'Verification % False Positive'])
        else:
            wr.writerow(['Classifier','Feature Set','Feature Count','Train Time','Eval Time','Run Time','% Correct','% False Positive'])
        for result in resultsListSorted:
            wr.writerow(result)
    print()

    bestRun = selectModel(resultsListSorted, args.falsepositives, args.percentdrop, verifydata != None and validationfolds > 0)

    # save classifier model file
    print("Saving model as {}.model...".format(filename))
    classifier = Classifier(classname=bestRun[0])
    dataset = genDataSetWithAttributes(data,bestRun[1])
    classifier.build_classifier(dataset)
    serialization.write_all(filename+"/"+filename+'.model',[classifier,Instances.template_instances(dataset)])

    print(classifier)

    jvm.stop()
    print("Done")
    exit()

def generateModels(classifierList,attributeCombos,data,classVal,verifydata=None,validationfolds=5,filename=None):
    """Generates and evaluates models for each classifier and feature combination"""
    resultsList = []
    try:
        print("Starting classification.\n")
        for className in classifierList:
            tempList = []
            print("Running "+className+"...")
            t = time.time()
            for indx, attrset in enumerate(attributeCombos):
                print("\rRunning on set {}".format(indx),end="",flush=True)
                dataset = genDataSetWithAttributes(data,attrset)
                vdataset = genDataSetWithAttributes(verifydata,attrset)
                result = runClassifier(className,dataset,validationfolds,vdataset)
                falsePositives = getFalsePositives(result[4],classVal)
                if verifydata != None and validationfolds > 0:
                    vfp = getFalsePositives(result[-1],classVal)
                    resultsList.append([className,attrset,len(attrset)]+result[0:4]+[100.0*falsePositives]+result[5:-1]+[100.0*vfp])
                    tempList.append([className,attrset,len(attrset)]+result[0:4]+[100.0*falsePositives]+result[5:-1]+[100.0*vfp])
                else:
                    resultsList.append([className,attrset,len(attrset)]+result[0:4]+[100.0*falsePositives])
                    tempList.append([className,attrset,len(attrset)]+result[0:4]+[100.0*falsePositives])
            print("\nDone ({})\n".format(time.time()-t))
            if filename != None:
                with open(filename+"/"+className+".csv",'w') as csvfile:
                    wr = csv.writer(csvfile,lineterminator='\n')
                    if verifydata != None and validationfolds > 0:
                        wr.writerow(['Classifier','Feature Set','Feature Count','Train Time','Eval Time','Run Time','% Correct','% False Positive',
                            'Verification % Correct', 'Verification % False Positive'])
                    else:
                        wr.writerow(['Classifier','Feature Set','Feature Count','Train Time','Eval Time','Run Time','% Correct','% False Positive'])
                    for temp in tempList:
                        wr.writerow(temp)
        print("Classifications complete\n")
    except KeyboardInterrupt:
        print("\nKeyboardInterrupt detected. Classification incomplete.")
    return resultsList

def getFeatureCombos(data,maxfeatures,minfeatures):
    """Generate a list of all possible combinations of attributes from the data, excluding class variable.
    Limits number of feature per combination by minfeature and maxfeatures"""

    print("Generating all possible combinations of attributes...")
    testnames = getAttributeNames(data)
    attributeCombos = []
    maxcount = len(testnames)
    mincount = 1
    if maxfeatures != None:
        maxcount = min([maxcount,maxfeatures])
    if minfeatures != None:
        mincount = max([mincount,minfeatures])
        mincount = min([mincount,maxcount])
    for ind in range(mincount,maxcount+1):
        t = time.time()
        testres = getFeatureNamesRec(testnames,ind)
        attributeCombos.extend(testres)
    print(len(attributeCombos))
    return attributeCombos

def selectModel(fullModelList, falsePositives, percentDrop, useVerification=False, data=None):
    """Uses model performance statistics and selection parameters to find the best model"""
    # Determine the best classifier runs and print them out
    # 1: Threshold by false positive %
    # 2: Threshold by maximum classification %
    # 3: Sort by number of features required
    # 4: Print highest classification rate classifier
    # 5: Take set from step 1 and threshold by % drop parameter
    # 6: Filter to classifiers with minimum number of features
    # 7: Print highest classification rate classifier
    try:
        if useVerification:
            modelList = [r for r in fullModelList if r[9] <= falsePositives]
            maxVal = max([r[8] for r in modelList])
            bestRuns = [r for r in modelList if r[8] == maxVal]
            bestRuns = sorted(bestRuns,key=lambda x: x[2])
            bestRun = bestRuns[0]
            print("Highest Classification Rate Run, based on verification data: ",end="")
            print(bestRun)
            bestRuns = [r for r in modelList if maxVal-r[8] <= percentDrop]
            minFtrs = min([r[2] for r in bestRuns])
            bestRuns = [r for r in bestRuns if r[2] == minFtrs]
            maxVal = max([r[8] for r in bestRuns])
            bestRuns = [r for r in bestRuns if r[8] == maxVal]
            bestRuns = sorted(bestRuns,key=lambda x: x[5])
            print("Best Run, based on verification data: ",end="")
            bestRun = bestRuns[0]
            print(bestRun)
        else:
            modelList = [r for r in fullModelList if r[7] <= falsePositives]
            maxVal = max([r[6] for r in modelList])
            bestRuns = [r for r in modelList if r[6] == maxVal]
            bestRuns = sorted(bestRuns,key=lambda x: x[2])
            bestRun = bestRuns[0]
            print("Highest Classification Rate Run, based on training data: ",end="")
            print(bestRun)
            bestRuns = [r for r in modelList if maxVal-r[6] <= percentDrop]
            minFtrs = min([r[2] for r in bestRuns])
            bestRuns = [r for r in bestRuns if r[2] == minFtrs]
            maxVal = max([r[6] for r in bestRuns])
            bestRuns = [r for r in bestRuns if r[6] == maxVal]
            bestRuns = sorted(bestRuns,key=lambda x: x[5])
            print("Best Run, based on training data: ",end="")
            bestRun = bestRuns[0]
            print(bestRun)
    except:
        print("No classifiers generated meet the given filter criteria.")
        if useVerification:
            print("Saving highest verification rate data.")
            bestRun = sorted(fullModelList,key=itemgetter(8))[-1]
        else:
            print("Saving highest classification rate data.")
            bestRun = sorted(fullModelList,key=itemgetter(6))[-1]
    print()
    if data != None:
        classifier = Classifier(classname=bestRun[0])
        dataset = genDataSetWithAttributes(data,bestRun[1])
        classifier.build_classifier(dataset)
        return classifier
    else:
        return bestRun

def runClassifier(className,data,folds,verificationdata=None):
    """Runs the given classifier with default parameters on the provided dataset"""
    classifier = Classifier(classname=className)

    class_caps = classifier.capabilities.class_capabilities().capabilities()
    class_caps = [cap.name for cap in class_caps]
    #if className == "weka.classifiers.functions.LDA" or className == "weka.classifiers.functions.QDA":
    if data.class_attribute.is_nominal and 'NOMINAL_CLASS' not in class_caps:
        # Ensure that binary classifiers will work with multiclass data
        classifier = Classifier(classname="weka.classifiers.meta.MultiClassClassifier")
        classifier.options = ["-W", className]
    
    classTime = time.time()
    classifier.build_classifier(data)
    classTime = time.time()-classTime

    evaluation = Evaluation(data)
    evalTime = time.time()
    if folds == 0:
        if verificationdata == None:
            evaluation.test_model(classifier,data)
        else:
            evaluation.test_model(classifier,verificationdata)
    else:
        evaluation.crossvalidate_model(classifier, data, folds, Random(time.time()))
    evalTime = time.time()-evalTime
    
    runTime = time.time()
    evaluation.test_model(classifier,data)
    runTime = (time.time()-runTime)/data.num_instances
	# 3rd parameter is number of cross-validation folds

    if verificationdata != None and folds > 0:
        verification = Evaluation(data)
        verification.test_model(classifier,verificationdata)
        # verification.evaluate_model(classifier,data)#verificationdata)
        return [classTime, evalTime, runTime, evaluation.percent_correct, evaluation.confusion_matrix,
            verification.percent_correct, verification.confusion_matrix]
    else:
        return [classTime, evalTime, runTime, evaluation.percent_correct, evaluation.confusion_matrix]

def getFeatureNamesRec(names,length,start=0):
    """Recusively generates a list of all combinations of feature names of a given length"""
    comboList = []
    for i in range(start,len(names)):
        if length==1:
            comboList.append([names[i]])
        else:
            tmp = getFeatureNamesRec(names,length-1,i+1)
            for t in tmp:
                t2 = [names[i]]
                t2.extend(t)
                comboList.append(t2)
    return comboList

def getAttributeNames(data):
    """Generates a list of attribute names from a WEKA dataset"""
    attrNames = []
    for attr in data.attributes():
        if attr.index != data.class_index:
            attrNames.append(attr.name)
    return attrNames

def genDataSetWithAttributes(data,attributes):
    """Filters out any attributes in the data that are not named in the provided attributes list"""
    if data == None:
        return None
    newdata = data.copy_instances(data)
    indx = 0
    while indx < newdata.num_attributes:
        attr = newdata.attribute(indx)
        if attr.name not in attributes and attr.name != newdata.class_attribute.name:
            newdata.delete_attribute(attr.index)
        else:
            indx = indx+1
    return newdata

def getFalsePositives(confusionMatrix, varIndex):
    """Calculates the rate of false positives given the confusion matrix and index of desired class value"""
    col = confusionMatrix[:,varIndex]
    total = float(sum(col))
    if total == 0.0:
        return 0.0
    correct = float(col[varIndex])
    perc = (total-correct)/total
    return perc

if __name__ == '__main__':
    main()