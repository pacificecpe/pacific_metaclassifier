import multiClassify
import timeFeatures
import meanOverlap
import csv
import argparse
import os
import time
import tempfile

import weka.core.jvm as jvm
from weka.core.classes import Random
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization

parser = argparse.ArgumentParser(description="determine the best classifier algorithm and feature set for a given set of data")
parser.add_argument("file", help="the data file")
parser.add_argument("-n", "--sensornames", type=str, help="list of sensor names in same order as source data")
parser.add_argument("-r", "--samplerate", type=float, default=1.0, help="rate at which sensors were sampled in Hz")
parser.add_argument("-vf","--verificationfile", type=str, help="second data file to be used for verification")
parser.add_argument("-cl","--classifierlist", type=str, help="file containing list of classifiers to be used, seperated by newline")
parser.add_argument("-mx","--maxfeatures", type=int, help="the maximum number of features to use per classifier")
parser.add_argument("-mn","--minfeatures", type=int, help="the minimum number of features to use per classifier")
parser.add_argument("-kf","--keepfeatures", type=float, default=0.5, help="the number of features to keep from mena overlap")
parser.add_argument("-p","--percentdrop", type=float, default=0.0, help="the percentage allowed to drop from max accuracy to find more efficient classifier")
parser.add_argument("-c","--classvar", type=str, help="the variable name to be used as the class")
parser.add_argument("-cc","--criticalclass", type=str, help="the class value to be used when calculating true/false positives")
parser.add_argument("-fp","--falsepositives", type=float, default=100.0, help="the maximum percent of allowed false positives")
parser.add_argument("-f","--folds", type=int, default=5, help="the number of folds to use during cross-validation")
parser.add_argument("-v","--verbose", default=False, action="store_true", help="enables java errors and extra error logging")
args = parser.parse_args()

filename = os.path.basename(args.file).split('.')[0]
test_file = args.file
# temp_file = "{}_temp.csv".format(filename)
with tempfile.NamedTemporaryFile(delete=False) as tf:
    temp_file = tf.name

print("Loading data file and extracting features...")
data = []
names = None
if args.sensornames != None:
    sensornames = args.sensornames.split(',')
else:
    sensornames = []
with open(test_file,'r') as csvfile:
    reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
    for row in reader:
        if names == None:
            names = timeFeatures.getFeatureNames(2,sensornames)
        data.append([row[0]]+timeFeatures.getFeatures(row[1:],2,args.samplerate))

with open(temp_file,'w') as csvfile:
    writer = csv.writer(csvfile,delimiter=',',lineterminator='\n')
    fieldnames = names
    writer.writerow(fieldnames)
    for d in data:
        writer.writerow(d)

print("Extracted {} features, filtering using mean overlap...".format(len(names)))
mo_data = meanOverlap.processData(data)
overlap = meanOverlap.getOverlaps(mo_data,names)
overlap = sorted(overlap,key=lambda x: x[1])
numtokeep = min(len(overlap),int(args.keepfeatures)) if args.keepfeatures > 1 else int(len(overlap)*args.keepfeatures)
if numtokeep < 0:
    numtokeep = int(len(overlap)*0.5)
goodFeatures = [o[0] for o in overlap[0:numtokeep]]
print("Top {} good features:".format(numtokeep))
print(goodFeatures)

jvm.start(packages=True)
loader = Loader(classname="weka.core.converters.CSVLoader")
data = loader.load_file(temp_file)
data.class_index = 0
data = multiClassify.genDataSetWithAttributes(data,goodFeatures)

classifierList = ["weka.classifiers.functions.LDA","weka.classifiers.functions.QDA","weka.classifiers.functions.MultilayerPerceptron",
                "weka.classifiers.trees.J48","weka.classifiers.trees.RandomTree","weka.classifiers.trees.RandomForest",
                "weka.classifiers.bayes.NaiveBayes","weka.classifiers.rules.DecisionTable","weka.classifiers.meta.RandomSubSpace",
                "weka.classifiers.functions.Logistic","weka.classifiers.functions.SimpleLogistic", "weka.classifiers.functions.LibSVM"]
featureCombos = multiClassify.getFeatureCombos(data,args.maxfeatures,args.minfeatures)
models = multiClassify.generateModels(classifierList,featureCombos,data,0,validationfolds=args.folds)
bestRun = multiClassify.selectModel(models,args.falsepositives,args.percentdrop)

classifier = Classifier(classname=bestRun[0])
dataset = multiClassify.genDataSetWithAttributes(data,bestRun[1])
classifier.build_classifier(dataset)
print(classifier)
print("\nSaving model as {}.model...".format(filename))
serialization.write_all(filename+'.model',[classifier,Instances.template_instances(dataset)])
jvm.stop()