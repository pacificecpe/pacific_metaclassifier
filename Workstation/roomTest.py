import sys
import serial
from timeFeatures import getFeatures
import weka.core.jvm as jvm
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD);
GPIO.setup(40,GPIO.OUT)
jvm.start(packages=True)
jobjects = serialization.read_all("2ClassMod.model")
dataset = Instances(jobject=jobjects[1])
print(dataset)
classifier = Classifier(jobject=jobjects[0])
print(classifier)
try:
	ser = serial.Serial('/dev/serial0',57600,timeout=1)
except:
	print("Serial error:", sys.exc_info()[0])
	exit()

try:
	data = []
	ser.readline()
	while True:
		line = ser.readline().decode('utf-8')
		newdata = [float(n) for n in line[0:-2].split(',')]
		# print(newdata)
		data += newdata
		if len(data) == 500:
			# print(data)
			# print('Extracting features...')
			data = getFeatures(data,2,['temp','light'],100)
			# print(data)
			# print('Mean temp:{:.2f}\tMean light:{:.2f}'.format(data[9],data[20]))
			inst = Instance.create_instance(['0',data[4]])
			inst.dataset = dataset
			res = classifier.classify_instance(inst)
			GPIO.output(40,int(res))
			print(res)
			data = []
except KeyboardInterrupt:
	print('Program aborted.')
ser.close()