import argparse
import os.path
import math
import numpy as np
import scipy
from scipy.signal import argrelextrema, argrelmax, argrelmin
from scipy.stats import skew, kurtosis
from collections import OrderedDict
import csv

def main():
    parser = argparse.ArgumentParser(description="determine classifiability of features in a dataset")
    parser.add_argument("file", help="the data file")
    parser.add_argument("-o","--maxoverlap", type=float, default=1.0, help="filter out features with higher mean overlap than this")
    args = parser.parse_args()
    print("Loading data file... ",end="")
    filename = os.path.basename(args.file).split('.')[0]
    names, dataset = loadFile(args.file)
    
    print("Done.")

    numclasses = int(max(dataset[:,0])+1)
    numfeatures = dataset.shape[1]
    print("Classes in data: {}\nFeatures: {}".format(numclasses,numfeatures))

    output = getOverlaps(dataset,names)
    
    from operator import itemgetter
    outputSorted = sorted(output,key=itemgetter(1))
    print("Overlaps:")
    for o in outputSorted:
        print("\t{} {:.2f}".format(o[0].ljust(20),100.0*o[1],2))

def loadFile(filename):
    """Load a CSV file with feature extracted data"""
    names = []
    classnames = []
    dataset = np.zeros([2, 2])
    with open(filename,'r') as csvfile:
        reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
        names = next(reader)
        i=0
        rows = [r for r in reader]
        dataset = np.zeros([len(rows), len(rows[0])])
        for row in rows:
            if row[0] not in classnames:
                classnames.append(row[0])
            row[0] = classnames.index(row[0])
            dataset[i,:] = np.array(row)
            i+=1
    return (names, dataset)

def processData(rows):
    """Convert list of samples into numpy matrix for processing"""
    names = []
    classnames = []
    dataset = np.zeros([2, 2])
    i=0
    dataset = np.zeros([len(rows), len(rows[0])])
    for row in rows:
        if row[0] not in classnames:
            classnames.append(row[0])
        row[0] = classnames.index(row[0])
        dataset[i,:] = np.array(row)
        i+=1
    return dataset

def getOverlaps(dataset, names):
    """Calculate the overlap between class space per feature"""
    numclasses = int(max(dataset[:,0])+1)
    numfeatures = dataset.shape[1]
    overlaps = np.zeros([numfeatures])
    output = []
    for f in range(1,numfeatures):
        vals = np.zeros([numclasses,100])
        for c in range(0,numclasses):
            vmean = dataset[dataset[:,0]==c,f]
            vmin = np.min(dataset[:,f])
            vmax = np.max(dataset[:,f])
            for j in range(0,len(vmean)):
                if vmax == vmin:
                    indx = 0
                else:
                    indx = min(99,int((vmean[j]-vmin)*(100/(vmax-vmin))))
                vals[c,indx] = vals[c,indx]+1
            vals[c,:] = vals[c,:]/len(vmean)
        
        sm = np.zeros([numclasses])
        su = np.zeros([numclasses])
        overlap = np.zeros([numclasses])
        for i in range(0,numclasses):
            for j in range(0,vals.shape[1]):
                sm[i] = sm[i]+vals[i,j]
                c = vals[i,j]>vals[:,j]
                if sum(c)==numclasses-1:
                    su[i] = su[i]+vals[i,j]-np.max(vals[c,j])
            overlap[i] = sm[i]-su[i]
        overlaps[f]=np.mean(overlap)
        output.append([names[f],overlaps[f]])
    return output

if __name__ == '__main__':
    main()