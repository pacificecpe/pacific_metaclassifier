import os
import time
import csv
import meanOverlap
import timeFeatures
import statistics

sourceFile = 'Work/3ClassTest_raw.csv'
resultsFile = 'Work/3ClassTest_results.csv'
outFile = 'Work/3ClassTest_results2.csv'

print("Loading data file and extracting features...")
data = []
names = None
sensornames = ['temp','light']
with open(sourceFile,'r') as csvfile:
    reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
    for row in reader:
        if names == None:
            names = timeFeatures.getFeatureNames(2,sensornames)
        data.append([row[0]]+timeFeatures.getFeatures(row[1:],2,20))

print("Extracted {} features, filtering using mean overlap...".format(len(names)))
mo_data = meanOverlap.processData(data)
overlap = meanOverlap.getOverlaps(mo_data,names)
fieldnam = [o[0] for o in overlap]
fielddat = [o[1] for o in overlap]
overlap = dict(zip(fieldnam,fielddat))

print("Reading results file...")
fieldnames = None
resultList = []
with open(resultsFile,'r') as csvfile:
    reader = csv.reader(csvfile,delimiter=',',lineterminator='\n')
    fieldnames = next(reader)
    for row in reader:
        features = (row[1])[2:-2].split("', '")
        ovlps = [overlap[f] for f in features]
        meanMO = statistics.mean(ovlps)
        medMO = statistics.median(ovlps)
        row.extend([meanMO,medMO,len(features),len(mo_data),len(features)/len(mo_data)])
        resultList.append(row)
fieldnames.extend(['Mean MO','Median MO','Num Attributes','Num Samples','Dimensionality'])

print("Saving modified results file...")
with open(outFile,'w') as csvfile:
    writer = csv.writer(csvfile,delimiter=',',lineterminator='\n')
    writer.writerow(fieldnames)
    for d in resultList:
        writer.writerow(d)