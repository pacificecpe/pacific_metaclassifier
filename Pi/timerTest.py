import time
import signal
import logging
import Adafruit_LSM303

logging.disable(logging.DEBUG)
lsm303 = Adafruit_LSM303.LSM303()

def timeout(a,b):
	lsm303.read()

x = 0
interval = 0.005

signal.signal(signal.SIGALRM,timeout)
signal.setitimer(signal.ITIMER_REAL,interval,interval)
print(time.time())
t1 = time.time()

while x<200:
	x = x + 1
	signal.pause()

total = time.time()-t1

signal.setitimer(signal.ITIMER_REAL,0,0)

print(total)