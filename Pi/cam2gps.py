import math
import Adafruit_BMP.BMP085 as BMP085
import gps
import time
import threading

altitude_offset = 70

glat = 0
glon = 0
altitude = 0

killthread = False
sensorThread = None

savedLat = 0
savedLon = 0
savedAlt = 0

def Init():
    killthread = False
    global sensorThread
    sensorThread = threading.Thread(target=UpdateSensors)
    sensorThread.daemon = True
    sensorThread.start()

def UpdateSensors():
    global glat, glon, altitude
    # Listen on port 2947 (gpsd) of localhost
    session = gps.gps("localhost", "2947")
    session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
    sensor = BMP085.BMP085()

    while True and not killthread:
        try:
            report = session.next()
            if report.keys()[0] == 'epx':
                #print 'Lat: {:3.6f}, Lon: {:3.6f}, Alt: {:3.2f}'.format(report['lat'],report['lon'],report['alt'])
                glat = report['lat']
                glon = report['lon']
                altitude = sensor.read_altitude()+altitude_offset
                #print 'Lat: {:3.6f}, Lon: {:3.6f}, Alt: {:3.2f}'.format(lat,lon,altitude)
                time.sleep(0.5)
        except KeyError:
            pass
        except KeyboardInterrupt:
            quit()

def OffsetGPSCoord(lat,lon,i,j):
    """Offsets the latitude and longitude by i and j meters"""
    nLat = lat + (180/math.pi)*i/6378137
    nLon = lon + (180/math.pi)*j/6378137*math.cos(math.radians(lat))
    return (nLat, nLon)

def GetWidth(focal, dist, pixelwidth):
    return (pixelwidth*dist)/focal
    
def GetGPS():
    """Saves current GPS coordinates for use in GetCoordinate"""
    global savedLat, savedLon, savedAlt
    savedLat = glat
    savedLon = glon
    savedAlt = altitude
    return glat != 0 and glon != 0
    
def GetCoordinate(imagewidth, pixeloffset_x, pixeloffset_y):
    """imagewidth is total original image width in pixels, pixeloffset is distance from center of image"""
    i = GetWidth(imagewidth,savedAlt,pixeloffset_y)
    j = GetWidth(imagewidth,savedAlt,pixeloffset_x)
    return OffsetGPSCoord(savedLat,savedLon,i,j)