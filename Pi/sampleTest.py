import time
import csv
import threading
import signal
import logging
import Adafruit_LSM303
from timeFeatures import getFeatures, getFeaturesDict
import weka.core.jvm as jvm
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization

def main():
	global lsm303
	logging.disable(logging.DEBUG)
	lsm303 = Adafruit_LSM303.LSM303()
	print("Accelerometer initialized")
	jvm.start(packages=True)
	jobjects = serialization.read_all("droptest.model")
	classifier = Classifier(jobject=jobjects[0])
	print(classifier)
	dataset = Instances(jobject=jobjects[1])
	print(dataset)
	
	sampleCount = 600
	sampleRate = 200.0
	full_data = []
	
	correct_count = 0
	bad_count = 0
	for i in range(0,5):
		raw_input("Navigate above soft target, then press enter...")
		print("Starting {} second data collection, drop now".format(sampleCount/sampleRate))
		data_sample = collectSamples(sampleCount,1/sampleRate)
		print("Collection complete, classifying...")
		data = getFeaturesDict(data_sample[1:-1],200)
		inst = Instance.create_instance([0,data["min"],data["variance"],data["skewness"],data["kurtosis"],data["mean"]])
		inst.dataset = dataset
		# print(inst)
		res = classifier.classify_instance(inst)
		res = dataset.class_attribute.value(res)
		print(res)
		if res == "soft":
			correct_count += 1
		else:
			bad_count += 1
		data_sample = ["soft",res]+data_sample
		full_data.append(data_sample)
	print("Soft target data collection complete, {} out of {} correct, move to hard target\n".format(correct_count,correct_count+bad_count))
	
	correct_count = 0
	bad_count = 0
	for i in range(0,5):
		raw_input("Navigate above hard target, then press enter...")
		print("Starting {} second data collection, drop now".format(sampleCount/sampleRate))
		data_sample = collectSamples(sampleCount,1/sampleRate)
		print("Collection complete, classifying...")
		data = getFeaturesDict(data_sample[1:-1],200)
		inst = Instance.create_instance([1,data["min"],data["variance"],data["skewness"],data["kurtosis"],data["mean"]])
		inst.dataset = dataset
		# print(inst)
		res = classifier.classify_instance(inst)
		res = dataset.class_attribute.value(res)
		print(res)
		if res == "hard":
			correct_count += 1
		else:
			bad_count += 1
		data_sample = ["hard",res]+data_sample
		full_data.append(data_sample)
	print("Hard target data collection complete, {} out of {} correct, done\n".format(correct_count,correct_count+bad_count))
	
	#do file saving
	# with open("rawdata_{}.csv".format(time.strftime("%Y.%m.%d_%H.%M.%S",time.gmtime())),'w') as csvfile:
		# wr = csv.writer(csvfile,lineterminator='\n')
		# for data in full_data:
			# wr.writerow(data)
	
	# print("Save complete, exiting")
	
	jvm.stop()
	
def collectSamples(samples, interval):
	data = []
	def callback(signum,frames):
		accel, mag = lsm303.read()
		data.append(accel[2])
	signal.signal(signal.SIGALRM,callback)
	signal.setitimer(signal.ITIMER_REAL,interval,interval)
	while len(data)<samples:
		signal.pause()
	signal.setitimer(signal.ITIMER_REAL,0,0)
	return data
	
if __name__ == "__main__":
	main()
