import subprocess
import smtplib
from email.mime.text import MIMEText
import datetime

# Change to your own account information
# Account Information
to = 'tristan.wattswillis@gmail.com' # Email to send to.
gmail_user = 'tristan.wattswillis@gmail.com' # Email to send from. (MUST BE GMAIL)
gmail_password = '' # Gmail password.
smtpserver = smtplib.SMTP('smtp.gmail.com', 587) # Server to use.

arg='ifconfig'  # Linux command to retrieve ip addresses.
# Runs 'arg' in a 'hidden terminal'.
p=subprocess.Popen(arg,shell=True,stdout=subprocess.PIPE)
data = p.communicate()  # Get data from 'p terminal'.

iplist = []
ifaces = data[0].split("\n\n")
for iface in ifaces:
	ifname = iface[0:6].strip()
	try:
		i1 = iface.index('inet addr:')
		i2 = iface.index(' ',i1+10)
	except:
		continue
	ifip = iface[i1+10:i2]
	iplist.append((ifname,ifip))
if len(iplist)==0:
	print "IP Address Emailer: No interfaces connected."
	exit(1)

smtpserver.ehlo()  # Says 'hello' to the server
smtpserver.starttls()  # Start TLS encryption
smtpserver.ehlo()
smtpserver.login(gmail_user, gmail_password)  # Log in to server
today = datetime.date.today()  # Get current time/date

text = ''
for ip in iplist:
	text += ip[0]+': '+ip[1]+'\n'
msg = MIMEText(text)
msg['Subject'] = 'IPs For RaspberryPi on %s' % today.strftime('%b %d %Y')
msg['From'] = gmail_user
msg['To'] = to
# Sends the message
smtpserver.sendmail(gmail_user, [to], msg.as_string())
# Closes the smtp server.
smtpserver.quit()

print "IP Address Emailer: Sent {0} interface addresses".format(len(iplist))