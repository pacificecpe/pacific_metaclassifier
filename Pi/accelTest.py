import time
import threading
import signal
import logging
import Adafruit_LSM303
from timeFeatures import getFeatures, getFeaturesDict
import weka.core.jvm as jvm
from weka.classifiers import Classifier, Evaluation
from weka.core.dataset import Instance, Instances
from weka.core.converters import Loader, Saver
import weka.core.serialization as serialization

logging.disable(logging.DEBUG)
lsm303 = Adafruit_LSM303.LSM303()

jvm.start(packages=True)
jobjects = serialization.read_all("paper_experiment_softhard.model")
dataset = Instances(jobject=jobjects[1])
print(dataset)
classifier = Classifier(jobject=jobjects[0])
print(classifier)
	
def collectSamples(samples, interval):
	data = []
	def callback(signum,frames):
		accel, mag = lsm303.read()
		data.append(accel[2])
	signal.signal(signal.SIGALRM,callback)
	signal.setitimer(signal.ITIMER_REAL,interval,interval)
	while len(data)<samples:
		signal.pause()
	signal.setitimer(signal.ITIMER_REAL,0,0)
	return data

starttime = time.time()
while True:
	data = collectSamples(500,0.002)
	
	print(time.time()-starttime)
	starttime = time.time()
	
	data = getFeaturesDict(data,500)
	print(data)
	inst = Instance.create_instance([1,data["skewness"]])
	inst.dataset = dataset
	print(inst)
	res = classifier.classify_instance(inst)
	print(res)
	# time.sleep(0.005)