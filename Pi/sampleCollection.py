import time
import csv
import threading
import signal
import logging
import Adafruit_LSM303
from timeFeatures import getFeatures, getFeaturesDict

def main():
	global lsm303
	logging.disable(logging.DEBUG)
	lsm303 = Adafruit_LSM303.LSM303()
	print("Accelerometer initialized")
	sampleCount = 600
	sampleRate = 200.0
	full_data = []
	for i in range(0,20):
		raw_input("Navigate above soft target, then press enter...")
		print("Starting {} second data collection, drop now".format(sampleCount/sampleRate))
		data_sample = collectSamples(sampleCount,1/sampleRate)
		data_sample = ["soft"]+data_sample
		full_data.append(data_sample)
		print("Collection complete\n")
	print("Soft target data collection complete, move to hard target\n")
	
	for i in range(0,0):
		raw_input("Navigate above hard target, then press enter...")
		print("Starting {} second data collection, drop now".format(sampleCount/sampleRate))
		data_sample = collectSamples(sampleCount,1/sampleRate)
		data_sample = ["hard"]+data_sample
		full_data.append(data_sample)
		print("Collection complete\n")
	print("Hard target data collection complete, saving to file...")
	
	#do file saving
	with open("rawdata_{}.csv".format(time.strftime("%Y.%m.%d_%H.%M.%S",time.gmtime())),'w') as csvfile:
		wr = csv.writer(csvfile,lineterminator='\n')
		for data in full_data:
			wr.writerow(data)
	
	print("Save complete, exiting")
	
	
def collectSamples(samples, interval):
	data = []
	def callback(signum,frames):
		accel, mag = lsm303.read()
		data.append(accel[2])
	signal.signal(signal.SIGALRM,callback)
	signal.setitimer(signal.ITIMER_REAL,interval,interval)
	while len(data)<samples:
		signal.pause()
	signal.setitimer(signal.ITIMER_REAL,0,0)
	return data
	
if __name__ == "__main__":
	main()
